<?php
/**
 * @file
 * timeline_drupal.constants.inc
 * Provides constants for the Timeline service.
 */

// Server constants.
define('TIMELINE_AUTHORIZE_URL', '/oauth/authorize');
define('TIMELINE_RESOURCE_URL', '/oauth/resource');
define('TIMELINE_TOKEN_URL', '/oauth/token');

// Client constants.
define('TIMELINE_RESPONSE_TYPE', 'code');
define('TIMELINE_CLIENT_CALLBACK_URL', 'timeline');
define('TIMELINE_FEED_URL', 'timeline/feed');
