Timeline Drupal
-------

Timeline Drupal module allows grabbing of information stored on Timeline API
hosted site. OAuth2 is used to communicate with the Timeline server and make
authentications. Authenticated accounts are stored in the separate table
({timeline_account}).
Timeline posts are stored into separate Drupal Content type, "Timeline". This
allows easy integration into Views and allows custom presentation options in
Drupal sites.

Installation
--------------------------------------------------------------------------------
Enable the module "timeline_drupal" from console:
  drush en timeline_drupal
or enable through GUI (admin/modules) page.

How to sign in with Timeline API credentials
--------------------------------------------------------------------------------
Required credentials / settings include:

1: Customer Key
2: Customer Secret
3: Timeline API URI (including http / https)

At the moment, those values could be obtained from your Timeline API server admin.
Each site should have those available prior to using the module.

How to configure the Timeline Drupal module
--------------------------------------------------------------------------------
1. Go to admin/config/services/timeline/settings.
2. Enter Timeline Host information, OAuth Consumer Key and OAuth Consumer secret
   "OAuth Consumer Key" and "OAuth Consumer Secret" needs to be obtained from
   Timeline administrator.
   "Timeline Host" should have complete URI to your Timeline API server (including
   http or https)
3. If needed, change the "Callback URL" (defaults to page /timeline/feed on your
   Drupal site)

 How to grab posts from Timeline server
 --------------------------------------------------------------------------------
 1. Go to admin/config/services/timeline in order to Authenticate your application
    (use "Authenticate" tab in the module settings).
 2. Enter name of the Application you would like to use to communicate with Timeline
    server.
    (Defaults to the URL of your Drupal site, with random numbers appended.)
 3. Authenticate the application. Press "Go to Timeline to add an authenticated account"
    button.
 4. Your will be forwarded to the Timeline site.
 5. Authenticate your application (pick one of the "Destination" feed from Timeline server).
 6. You will be forwarded back to your Drupal site. Posts should appear.

How to set Cron tasks
--------------------------------------------------------------------------------
1. Open Cron configuration options (admin/config/services/timeline/cron)
2. Define Cron options: "Number of items to add to the queue".
3. Define desired Cron interval (defaults to 1 hour).
4. Save Cron settings.

Run cron tasks manually
--------------------------------------------------------------------------------
1. Open Cron configuration options (admin/config/services/timeline/cron)
2. After setting authenticated account (in the admin/config/services/timeline),
   you will be allowed to run the cron and import posts into your Drupal site.


Credits / Contact
--------------------------------------------------------------------------------
Currently maintained by Web Development Services (WDS), Academic Technology Services
of the Princeton University.

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
TODO: define project location
  https://<TO BE SET>


References
--------------------------------------------------------------------------------
1: http://timeline-info.princeton.edu/
