<?php
/**
 * @file
 * PWDS widgets Template.
 *
 * PWDS widget template - show Timeline items.
 * PWDS (or external) website.
 */
?>
<div class="pwds_widgets news">
  <?php
  if (isset($content) && !empty($content) && isset($settings)) {
    // Array for the Content.
    $show_readmore = (isset($settings['timeline_drupal_block_show_read_more']) && $settings['timeline_drupal_block_show_read_more']) ? TRUE : FALSE;
    $show_summary = (isset($settings['timeline_drupal_block_show_summary']) && $settings['timeline_drupal_block_show_summary']) ? TRUE : FALSE;
    $show_category = (isset($settings['timeline_drupal_block_show_tags']) && $settings['timeline_drupal_block_show_tags']) ? TRUE : FALSE;
    $show_speaker = (isset($settings['timeline_drupal_block_show_speaker']) && $settings['timeline_drupal_block_show_speaker']) ? TRUE : FALSE;
    $show_audience = (isset($settings['timeline_drupal_block_show_audience']) && $settings['timeline_drupal_block_show_audience']) ? TRUE : FALSE;
    $show_location = (isset($settings['timeline_drupal_block_show_location']) && $settings['timeline_drupal_block_show_location']) ? TRUE : FALSE;
    $show_date = (isset($settings['timeline_drupal_block_show_date']) && $settings['timeline_drupal_block_show_date']) ? TRUE : FALSE;
    $count_limit = (isset($settings['timeline_drupal_block_node_count']) && $settings['timeline_drupal_block_node_count']) ? $settings['timeline_drupal_block_node_count'] : 5;
    $data_format = (isset($settings['data_format'])) ? $settings['data_format'] : 'M jS, Y g:i a';

    // Grab the data.
    $counter = 0;
    foreach ($content as $item) {
      $data = '';
      $counter++;
      if (is_object($item) && $counter <= $count_limit) {
        $body = $item->summary;

        // Title.
        $title = $item->title;

        // URL on Timeline site.
        $id = $item->id;

        // URL on Drupal.
        $post_link = url(drupal_get_path_alias('node/' . $item->nid));

        if (isset($item->urlRef) && !empty($item->urlRef)) {
          $post_link = $item->urlRef;
        }

        $start_time = $item->startTime;
        $end_time = $item->endTime;
        $location = $item->location;
        $speaker = $item->speaker;
        $audience = $item->audience;

        // Location Link (if present).
        if (isset($item->locationRef) && !empty($item->locationRef)) {
          $location_uuid = $item->locationRef->field_timeline_location_uuid;
          $location_link = 'https://m.princeton.edu/map/campus?feed=91eda3cbe8&featureindex=' . $location_uuid[LANGUAGE_NONE][0]['value'];
          $location_title = $item->locationRef->name;
        }

        // Location Detail.
        $location_detail = (isset($item->locationDetail) && !empty($item->locationDetail)) ? $item->locationDetail : '';

        $canceled = $item->canceled;
        $canceled_reason = $item->canceledReason;
        $canceled_text = (!empty($canceled) && ($canceled == TRUE || $canceled == 1)) ? "<br/><em>Canceled : $canceled_reason</em>" : "";

        // Get Tags.
        $location_tags = $item->locationId;
        $categories_tags = $item->tagSpec;

        if (!empty($title)) {
          ?>
          <div class="news_title">
            <a
              href="<?php print check_url($post_link); ?>">
              <?php print check_plain($title); ?>
            </a>
            <?php print render($canceled_text); ?>
          </div>
          <?php
          // Date.
          if ($show_date == TRUE) {
            ?>
            <div class="news_date">
              <span class="date-display-single date-display-start">
                <?php
                // Date.
                if (!empty($start_time)) {
                  $date = strtotime($start_time);
                  $tmp = date($data_format, $date);
                  print render($tmp);
                }
                if (!empty($end_time)) {
                  $date = strtotime($end_time);
                  $tmp = ' - ' . date($data_format, $date);
                  print render($tmp);
                }
                ?>
                </span>
            </div>
            <?php
          }

          // Summary / Body.
          if ($show_summary) {
            ?>
            <div class="news_summary">
              <?php
              // Print Body.
              print render($body);
              ?>
            </div>
            <?php
          }
          // Speaker.
          if ($show_speaker) {
            ?>
              <div class="news_speaker">
                <span class="timeline-speaker-label">Speaker:</span>
                <?php print render($speaker); ?>
              </div>
            <?php
          }
          // Audience.
          if ($show_audience) {
            ?>
              <div class="news_audience">
                <span class="timeline-audience-label">Audience: </span>
                <?php print render($audience); ?>
              </div>
            <?php
          }

          // Location.
          if ($show_location && (!empty($location) || !empty($location_tags))) {
            ?>
            <div class="events_location">Location:
              <?php
              // Location link (if exists).
              if (isset($location_link) && !empty($location_link)) {
                ?>
                <a href="<?php echo $location_link; ?>" target="_blank">
                  <?php
                  if (!empty($location_tags)) {
                    $tmp = $location_tags;
                    print render($tmp);
                  }
                  else {
                    print render($location);
                  }
                  ?>
                </a>
                <?php
              }
              // Location Detail (if exists).
              if (!empty($location_detail)) {
                print ' (' . render($location_detail) . ')' . '<br/>';
              }
              ?>
            </div>
            <?php
          }

          // Category.
          if ($show_category == TRUE && !empty($categories_tags)) {
            $tmp = $categories_tags;
            ?>
            <div class="news_category">
              Tags: <?php print render($tmp); ?>
            </div>
            <?php
          }

          // Read More Link.
          if ($show_readmore && isset($body) && $body != '') {
            ?>
            <div class="views-field views-field-view-node">
              <span class="field-content">
            <?php
            $stripped_title = strip_tags($title);
            $tmp = '<a href="' . ($post_link) . '" title="' . $stripped_title . '">Read more...</a></span>';
            print render($tmp);
            ?>
              </span>
            </div>
            <?php
          }
          ?>
          <br/>
          <?php
        }
      }
    }
  }
  else {
    ?>
    No results in the category.
    <?php
  }
  ?>
</div>
