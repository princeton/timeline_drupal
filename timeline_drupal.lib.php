<?php
/**
 * @file
 * Integration layer to communicate with the Timeline API.
 */

/**
 * Exception handling class.
 */
class TimelineException extends Exception {
}

/**
 * Implements Taxonomy Tags for Timeline.
 * Class TimelineTags
 */
class TimelineTags {
  protected $vocabulary;
  protected $term;

  public function __construct($vocabulary) {
    $this->vocabulary = $vocabulary;
  }

  /**
   * Add new Term to the existing Vocabulary.
   * @param $terms
   * @param $vocabulary
   * @return array
   */
  public function add_term($terms, $vocabulary, $parent_tid = NULL) {
    return _timeline_drupal_add_taxonomy($terms, $vocabulary, $parent_tid);
  }

  /**
   * Store array of Taxonomy terms recursively.
   * @param $vocab
   * @param $vid
   * @param null $parent
   */
  public function import_terms($vocab, $vid, $parent = null) {
    foreach ($vocab as $term_name => $term) {
      // Create the term
      $term_object = new stdClass();
      $term_object->vid = $vid;
      $term_object->name = $term_name;
      if (isset($parent)) {
        $term_object->parent = $parent;
      }
      taxonomy_term_save($term_object);

      if (is_array($term)) {
        // Recursively call the function, passing the term id of the parent.
        import_terms($term, $vid, $term_object->tid);
      }
    }
  }

  public function fetch_taxonomy($url) {
    $timeline = new Timeline();
    $url = 'https://timeline.princeton.edu/public/tagtree';
    $data = $timeline->curl_get($url);

    // TODO: populate the data with complete Taxonomy.
    // Tagtree.
  }
}

/**
 * Primary Timeline API implementation class
 */
class Timeline {
  /**
   * @var $source the timeline api 'source'
   */
  protected $source;
  protected $consumer;
  protected $token;
  protected $state;

  /**
   * @var Config settings.
   */
  protected $config;

  /**
   * Constructor for the Timeline class
   */
  public function __construct($consumer_key = NULL, $consumer_secret = NULL, $redirect_url = NULL, $state = NULL) {
    if (empty($consumer_key)) {
      $consumer_key = variable_get('timeline_drupal_consumer_key');
    }
    if (empty($consumer_secret)) {
      $consumer_secret = variable_get('timeline_drupal_consumer_secret');
    }

    $this->config = $this->_set_config($consumer_key, $consumer_secret, $redirect_url);
  }

  /**
   * Set Config options.
   * @param $consumer_key
   * @param $consumer_secret
   * @param $redirect_url
   * @param $state
   * @param string $grant_type
   * @param string $response_type
   * @return array
   */
  public function _set_config($consumer_key, $consumer_secret, $redirect_url, $state = NULL, $grant_type = 'client_credentials', $response_type = TIMELINE_RESPONSE_TYPE) {

    // Set default value for OAuth State.
    if (empty($state)) {
      $client_url = $_SERVER['SERVER_NAME'];
      $state = variable_get('timeline_drupal_app_name', uniqid($client_url . '_'));
    }

    $params = array(
      "client_id" => $consumer_key,
      "client_secret" => $consumer_secret,
      "grant_type" => $grant_type,
      "redirect_uri" => url($redirect_url, array('absolute' => TRUE)),
      "response_type" => $response_type,
      "state" => $state
    );
    return $params;
  }

  /**
   * Get value of Code param.
   * @param null $host
   * @return null|type
   */
  public function get_code_val($host = NULL) {
    // Get Code.
    $code = NULL;
    $timeline_drupal_oauth_val = variable_get('timeline_drupal_oauth_code_val');

    if ((isset($_REQUEST) && key_exists('code', $_REQUEST)) || (!empty($timeline_drupal_oauth_val))) {
      if (isset($_REQUEST) && key_exists('code', $_REQUEST)) {
        $request = ($_REQUEST);
        $code = _timeline_drupal_get_code($request);
      }
      elseif (!empty($timeline_drupal_oauth_val)) {
        $code = $timeline_drupal_oauth_val;
      }
    }

    if ($code == NULL || $code == '') {
      $timelineUser = new TimelineUser();
      $values = $timelineUser->get_account_data($host);
      $code = $values['code'];
    }

    variable_set('timeline_drupal_oauth_code_val', $code);

    return $code;
  }

  /**
   * Get value of State param.
   * @param null $host
   * @return null
   */
  public function get_state_val($host = NULL) {
    // Get State.
    $state = NULL;
    $timeline_drupal_oauth_state_val = variable_get('$timeline_drupal_oauth_state_val');
    if ((isset($_REQUEST) && key_exists('state', $_REQUEST)) || (!empty($timeline_drupal_oauth_state_val))) {
      if (isset($_REQUEST) && key_exists('state', $_REQUEST)) {
        $request = $_REQUEST;
        $state = filter_xss($request['state']);
      }
      elseif (!empty($timeline_drupal_oauth_state_val)) {
        $state = $timeline_drupal_oauth_state_val;
      }
    }

    if ($state == NULL || $state == '') {
      $timelineUser = new TimelineUser();
      $values = $timelineUser->get_account_data($host);
      $state = filter_xss($values['state']);
    }

    variable_set('$timeline_drupal_oauth_state_val', $state);

    return $state;
  }

  /**
   * Get value of Access / Refresh Token params (if already set).
   * @param null $host
   * @return null
   */
  public function get_token_val($host = NULL) {
    // Get Access Token (if already set).
    $access_token = NULL;
    $refresh_token = NULL;
    $expires_in = time() + 60;

    $access_token_var = variable_get('timeline_drupal_oauth_access_token_val');
    $refresh_token_var = variable_get('timeline_drupal_oauth_refresh_token_val');
    $expires_in_var = variable_get('timeline_drupal_oauth_token_expire');

    // Get Access Token from the Session.
    if ((isset($_REQUEST) && key_exists('access_token', $_REQUEST)) || (!empty($access_token_var))) {
      // Access Token.
      if (isset($_REQUEST) && key_exists('access_token', $_REQUEST)) {
        $request = ($_REQUEST);
        $access_token = check_plain($request['access_token']);
      }
      elseif (!empty($access_token_var)) {
        $access_token = $access_token_var;
      }
    }

    // Get Refresh Token from the Session.
    if ((isset($_REQUEST) && key_exists('refresh_token', $_REQUEST)) || (!empty($refresh_token_var))) {
      // Refresh Token.
      if (isset($_REQUEST) && key_exists('refresh_token', $_REQUEST)) {
        $request = ($_REQUEST);
        $refresh_token = check_plain($request['refresh_token']);
      }
      elseif (!empty($refresh_token_var)) {
        $refresh_token = $refresh_token_var;
      }
    }

    // Get Expires in value from the Session.
    if ((isset($_REQUEST) && key_exists('expires_in', $_REQUEST)) || (!empty($expires_in_var))) {
      // Expires in value.
      if (isset($_REQUEST) && key_exists('expires_in', $_REQUEST)) {
        $request = ($_REQUEST);
        $expires_in = time() + check_plain($request['expires_in']) - 60;
      }
      elseif (!empty($expires_in_var)) {
        $expires_in = $expires_in_var;
      }
    }

    // Get from DB if empty.
    if (empty($access_token) || empty($refresh_token)) {
      // Check if already in DB.
      $timelineUser = new TimelineUser();
      $values = $timelineUser->get_account_data($host);
      $access_token = (!empty($values['access_token'])) ? $values['access_token'] : NULL;
      $refresh_token = (!empty($values['refresh_token'])) ? $values['refresh_token'] : NULL;
    }

    // Tokens.
    $tokens = array(
      'access_token' => $access_token,
      'refresh_token' => $refresh_token,
      'expires_in' => $expires_in,
    );

    return $tokens;
  }

  /********************************************
   * CURL Requests / Authentication.
   ********************************************/

  /**
   * POST request via Curl.
   * @param $url
   * @param $fields
   * @return mixed
   */
  public function curl_post($url, $fields) {
    $ch = curl_init();
    // Set the url, number of POST vars, POST data.
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, count($fields));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, _timeline_drupal_urlify($fields));
    curl_setopt($ch, CURLOPT_TIMEOUT, 3);
    // Execute post.
    $result = curl_exec($ch);
    // Close connection.
    curl_close($ch);
    return $result;
  }

  /**
   * POST request via Curl.
   * @param $url
   * @param $access_token
   * @return mixed
   */
  public function curl_get($url, $access_token = NULL, $params = NULL) {
    $ch = curl_init();

    $authorization = NULL;
    if (!empty($access_token)) {
      $authorization = "Authorization: Bearer $access_token";
    }

    // Additional parameters (if present).
    if (!empty($params)) {
      module_load_include('inc', 'timeline_drupal');
      $url .= '?' . _timeline_drupal_urlify($params);
    }

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Accept: application/json',
      $authorization
    ));
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 3);
    $result = curl_exec($ch);
    curl_close($ch);

    watchdog('timeline_drupal', 'Fetching the data from %url, using token: %token', array(
      '%url' => $url,
      '%token' => $access_token
    ), WATCHDOG_INFO);
    return $result;
  }

  /********************************************
   * Parameters.
   ********************************************/

  /**
   * Prepare the params for Authorization.
   * @param $state
   * @return array
   */
  private function get_authorize_params($state) {
    $params = array(
      "client_id" => variable_get('timeline_drupal_consumer_key'),
      "grant_type" => "client_credentials",
      "redirect_uri" => urlencode(TIMELINE_CLIENT_CALLBACK_URL),
      "response_type" => RESPONSE_TYPE,
      "state" => $state
    );
    return $params;
  }

  /********************************************
   * Fetching Data.
   ********************************************/

  /**
   * Fetch Timeline feed data.
   * @param $access_token
   * @param $refresh_token
   * @param null $params
   *  $params - array with values: since, start, end (Unix Timestamp).
   *  Fetches the data with dates that belongs to only those constraints.
   * @return null
   */
  public function fetch_feed($access_token, $refresh_token, $params = NULL) {
    $url = variable_get('timeline_drupal_host') . TIMELINE_RESOURCE_URL;

    // Prepare params (since, start, end).
    // .../oauth/resource?since=123456789&start=123456789&end=123456789 .
    // The "since" parameter will restrict it to only send items that have been
    // changed/added since the given timestamp.
    // The "start" and "end" parameters define the event date range.
    // Note that the start and end values work such that you'll get all events
    // that overlap the specified time period at all - that is, all events that
    // start sometime before the value of "end", and that end sometime after the
    // value of "start".

    $feed_parameters = NULL;
    if (is_array($params) && !empty($params)) {
      module_load_include('inc', 'timeline_drupal');
      $feed_parameters = _timeline_drupal_urlify($params);
    }

    $expires_in = variable_get('timeline_drupal_oauth_token_expire');
    $token_expired = FALSE;
    if (time() >= $expires_in) {
      $token_expired = TRUE;
    }

    if ($token_expired == FALSE) {
      $response = $this->curl_get($url, $access_token, $feed_parameters);
      $content = json_decode($response);
      $data = NULL;
    }

    // In case of token errors, refresh the token, then fetch the data.
    if ((isset($content) && isset($content->error) && ($content->error == 'expired_token' || $content->error == 'invalid_token')) || $token_expired == TRUE) {
      watchdog('timeline_drupal', 'Re-authenticating application - access token expired (token: %val).', array('%val' => $access_token), WATCHDOG_ERROR);

      // Re-Authenticate.
      $timelineUser = new TimelineUser();
      $response = $timelineUser->reauthenticate($access_token, $refresh_token);
      // Re-authenticate, save user info to DB.
      $access_token = $timelineUser->update_auth($access_token, $refresh_token, $response);

      // Get the data from the server again, with new access_token.
      $response = $this->curl_get($url, $access_token, $feed_parameters);
      $content = json_decode($response);
    }


    if (isset($content->success) && $content->success == 1) {
      $data = $content->items;
      $this->get_destination_names($content);
      try {
        $timelineStatus = new TimelineStatus();
        foreach ($data as $status) {
          $clear_cache = $timelineStatus->save($status);
          if ($clear_cache == TRUE || $clear_cache == 1) {
            // Clear all the related cache.
            if (function_exists('_timeline_drupal_clear_cache_node')) {
              $query = new EntityFieldQuery();
              $query->entityCondition('entity_type', 'node')
                ->entityCondition('bundle', 'timeline')
                ->fieldCondition('field_timeline_uuid', 'value', $status->id)
                ->range(0, 1)
                ->addMetaData('account', user_load(1));

              $result = NULL;
              $result = $query->execute();

              if (!empty($result['node'])) {
                $tmp = array_keys($result['node']);
                $node_item = array_shift($tmp);
                $node = node_load($node_item);
                _timeline_drupal_clear_cache_node($node);
              }
            }
          }
        }
      } catch (Exception $ex) {
        // Pass errors on saving.
        if (isset($status->id)) {
          watchdog('timeline_drupal', 'Error on saving status: %uid', array('%uid' => $status->id), WATCHDOG_ERROR);
        }
        else {
          // Error saving status with given Access_token.
          watchdog('timeline_drupal', 'Error on saving status (token: %val)', array('%val' => $access_token), WATCHDOG_ERROR);
        }
      }
    }

    return $data;
  }

  /**
   * Gets names of Timeline API Destinations (feed names).
   * @param $content
   */
  public function get_destination_names($content) {
    $destinations = array();
    foreach ($content->destinations as $destination) {
      $destinations[] = $destination->name;
    }
    variable_set('timeline_drupal_destinations', implode('; ', $destinations));
  }

  /**
   * Returns authentication URL.
   * @param $token
   * @return string
   */
  public function get_authenticate_url($token) {
    $url = variable_get('timeline_drupal_host') . '/oauth/authenticate';
    $url .= '?oauth_token=' . $token['oauth_token'];
    return $url;
  }

  /**
   * Grabs specific token from the JSON response.
   * @param $response
   * @param string $token_name
   * @return null
   */
  public function get_token($response, $token_name = 'access_token') {
    $json = json_decode($response, TRUE);
    return (isset($json) && key_exists($token_name, $json)) ? $json[$token_name] : NULL;
  }

  /**
   * Request an access token to the Timeline API.
   *
   * @param string $code
   *   String Code to append to the request or NULL.
   * @return
   *   String the access token or FALSE when there was an error.
   */
  public function fetch_access_token($code) {
    $access_token = NULL;

    $token_params = array(
      'client_id' => variable_get('timeline_drupal_consumer_key'),
      'client_secret' => variable_get('timeline_drupal_consumer_secret'),
      'grant_type' => 'authorization_code',
      'code' => $code,
      'redirect_uri' => url('/admin/config/services/' . variable_get('timeline_drupal_oauth_callback_url', TIMELINE_CLIENT_CALLBACK_URL), array('absolute' => TRUE)),
    );

    $url = variable_get('timeline_drupal_host') . TIMELINE_TOKEN_URL;
    $response = $this->curl_post($url, $token_params);

    return $response;
  }

  /**
   * Performs a request.
   *
   * @throws TimelineException
   */
  protected function request($url, $params = array(), $method = 'GET') {
    $data = '';
    if (count($params) > 0) {
      if ($method == 'GET') {
        $url .= '?' . http_build_query($params, '', '&');
      }
      else {
        $data = http_build_query($params, '', '&');
      }
    }

    $headers = array();

    $headers['Authorization'] = 'Oauth';
    $headers['Content-type'] = 'application/x-www-form-urlencoded';

    $response = $this->doRequest($url, $headers, $method, $data);
    if (!isset($response->error)) {
      return $response->data;
    }
    else {
      // Extract response error.
      $error = $response->error;
      // See if there is an error message in the response's data.
      // This will be an error message from the Timeline API.
      if (isset($response->data)) {
        $data = $this->parse_response($response->data);
        if (isset($data['error'])) {
          $error .= "\n" . $data['error'];
        }
      }
      throw new TimelineException($error);
    }
  }

  /**
   * Actually performs a request.
   *
   * This method can be easily overriden through inheritance.
   *
   * @param string $url
   *   The url of the endpoint.
   * @param array $headers
   *   Array of headers.
   * @param string $method
   *   The HTTP method to use (normally POST or GET).
   * @param array $data
   *   An array of parameters
   * @return
   *   stdClass response object.
   */
  protected function doRequest($url, $headers, $method, $data) {
    return drupal_http_request($url, array(
      'headers' => $headers,
      'method' => $method,
      'data' => $data
    ));
  }

  /**
   * @see https://www.drupal.org/node/985544
   */
  protected function parse_response($response) {
    $length = strlen(PHP_INT_MAX);
    $response = preg_replace('/"(id|in_reply_to_status_id|in_reply_to_user_id)":(\d{' . $length . ',})/', '"\1":"\2"', $response);
    return json_decode($response, TRUE);
  }

  /**
   * Get an array of TimelineStatus objects from an API endpoint.
   */
  protected function get_statuses($path, $params = array()) {
    $values = $this->call($path, $params, 'GET');
    // Check on successfull call.
    if ($values) {
      $statuses = array();
      foreach ($values as $status) {
        $statuses[] = new TimelineStatus($status);
      }
      return $statuses;
    }
    // Call might return FALSE, e.g. on failed authentication.
    else {
      // As call already throws an exception, we can return an empty array to
      // break no code.
      return array();
    }
  }

  /**
   * Destroys the status specified by the required ID parameter.
   *
   * @param array $params
   *   an array of parameters.
   *
   * @return
   *   TimelineStatus object if successful or FALSE.
   */
  public function statuses_destroy($id, $params = array()) {
    $values = $this->call('statuses/update', $params, 'POST');
    if ($values) {
      return new TimelineStatus($values);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Updates the Timeline posts.
   *
   * @param string $status
   *   The text of the status update (the post).
   * @param array $params
   *   an array of parameters.
   */
  public function statuses_update($status, $params = array()) {
    $params['status'] = $status;
    $values = $this->call('statuses/update', $params, 'POST');
    return new TimelineStatus($values);
  }
}

/**
 * Class for containing an individual timeline status.
 */
class TimelineStatus {
  /**
   * @var created_time
   */

  public $uuid;
  public $created_time;
  public $title;
  public $content;
  public $location;
  public $location_id;
  public $location_detail;
  public $url_ref;
  public $timezone;
  public $start_time;
  public $end_time;
  public $duration;
  public $frequency;
  public $increment;
  public $until_date;
  public $day_index;
  public $exclusions;
  public $from;
  public $tag_spec;
  public $canceled;
  public $canceled_reason;
  public $reply_to;
  public $entities;

  // Last and Next sync .
  private $date_last_sync_var;
  private $date_next_sync_var;

  /**
   * Constructor for TimelineStatus
   */
  public function __construct($values = array()) {
    foreach ($values as $key => $val) {
      $this->$key = $val;
    }
  }

  /***
   * Sets last execution time value.
   * @param null $time
   */
  public function setDateLastExecutionVal($time = NULL) {
    if ($time == NULL) {
      $time = time();
    }

    $this->date_last_sync_var = $time;
    variable_set('timeline_drupal_last_execution', $time);
  }

  /**
   * Sets next execution time value.
   * @param null $time
   */
  public function setDateNextExecutionVal($time = NULL) {
    if ($time == NULL) {
      $time = time();
    }

    $this->date_next_sync_var = $time + 60 * 15;
    variable_set('timeline_drupal_next_execution', $this->date_next_sync_var);
  }

  /**
   * Gets last execution time value.
   * @return mixed
   */
  public function getDateLastExecutionVal() {
    return variable_get('timeline_drupal_last_execution', NULL);
  }

  /**
   * Gets next execution time value.
   * @return mixed
   */
  public function getDateNextExecutionVal() {
    return variable_get('timeline_drupal_next_execution', NULL);
  }

  /**
   * Loads latest items from the feed.
   * @param null $param
   */
  public function load($params = NULL) {
    module_load_include('inc', 'timeline_drupal');
    $url = variable_get('timeline_drupal_host') . TIMELINE_RESOURCE_URL . '?' . _timeline_drupal_urlify($params);

    $client_host = $_SERVER['SERVER_NAME'];

    $timeline = new Timeline();
    $tokens = $timeline->get_token_val($client_host);
    $access_token = (!empty($tokens['access_token'])) ? $tokens['access_token'] : variable_get('timeline_drupal_oauth_access_token_val');
    $refresh_token = (!empty($tokens['refresh_token'])) ? $tokens['refresh_token'] : variable_get('timeline_drupal_oauth_refresh_token_val');
    $expires_in = (!empty($tokens['expires_in'])) ? $tokens['expires_in'] : variable_get('timeline_drupal_oauth_token_expire');

    if (empty($url) || empty($access_token)) {
      $items = array();
    }
    else {
      $feed_parameters = NULL;
      if (is_array($params) && !empty($params)) {
        $feed_parameters = _timeline_drupal_urlify($params);
      }

      $current_time = time();

      // Check if token still active.
      $still_active_token = TRUE;
      if ($current_time >= $expires_in) {
        $still_active_token = FALSE;
      }

      if ($still_active_token == TRUE) {
        $timeline = new Timeline();
        $response = $timeline->curl_get($url, $access_token, $feed_parameters);
        $this->setDateLastExecutionVal($current_time);
        $this->setDateNextExecutionVal($current_time);
        $items = json_decode($response);
      }

      if ((isset($items->error) && ($items->error == 'expired_token' || $items->error == 'invalid_token')) || $still_active_token == FALSE) {
        // Refresh Token.
        watchdog('timeline_drupal', 'Re-authenticating application - access token expired (token: %val).', array('%val' => $access_token), WATCHDOG_ERROR);

        // Re-Authenticate.
        $timelineUser = new TimelineUser();
        $response = $timelineUser->reauthenticate($access_token, $refresh_token);
        // Re-authenticate, save user info to DB.
        $access_token = $timelineUser->update_auth($access_token, $refresh_token, $response);

        // Get the data from the server again, with new access_token.
        $this->setDateLastExecutionVal($current_time);
        $this->setDateNextExecutionVal($current_time);
        $response = $timeline->curl_get($url, $access_token, $feed_parameters);
        $items = json_decode($response);
      }
    }
    return $items;
  }

  /**
   * Retrieve data from either DB or Timeline API.
   *
   * To be used by the Widget.
   */
  public function retrieve($items_to_show = NULL, $cache_live = 15) {
    module_load_include('lib.php', 'timeline_drupal');
    $timelineStatus = new TimelineStatus();

    // Get next execution (cron) time.
    $next_execution = variable_get('timeline_drupal_next_execution', time());
    $last_execution = $this->getDateLastExecutionVal();

    if (time() >= $next_execution || (isset($cache_live) && time() >= ($last_execution + 60 * $cache_live))) {
      // Load the data from Timeline API.
      // Add in the content.
      $resource_url = variable_get('timeline_drupal_host');
      if (!empty($resource_url)) {
        // Read from Feed from API.
        $load_param_val = $this->getDateLastExecutionVal();
        $load_param = NULL;
        if ($load_param_val != NULL) {
          $load_param = array('since' => $load_param_val);
        }
        $content = $timelineStatus->load($load_param);

        // Save to DB.
        if (isset($content->success) && $content->success == 1) {
          $data = $content->items;
          $data = (array) $data;
          // Save the data to DB.
          foreach ($data as $status) {
            $clear_cache = $timelineStatus->save($status);
            if ($clear_cache == TRUE || $clear_cache == 1) {
              // Clear all the related cache.
              if (function_exists('_timeline_drupal_clear_cache_node')) {
                $query = new EntityFieldQuery();
                $query->entityCondition('entity_type', 'node')
                  ->entityCondition('bundle', 'timeline')
                  ->fieldCondition('field_timeline_uuid', 'value', $status->id)
                  ->range(0, 1)
                  ->addMetaData('account', user_load(1));

                $result = NULL;
                $result = $query->execute();

                if (!empty($result['node'])) {
                  $tmp = array_keys($result['node']);
                  $node_item = array_shift($tmp);
                  $node = node_load($node_item);
                  _timeline_drupal_clear_cache_node($node);
                }
              }
            }
          }
          $cron_interval = variable_get('timeline_drupal_interval', 60 * 60);
          variable_set('timeline_drupal_next_execution', time() + $cron_interval);
        }
      }
    }
    // Pull the data from DB.
    $data = $timelineStatus->get($items_to_show);
    module_load_include('inc', 'timeline_drupal');
    $data = _timeline_drupal_convert_list($data);

    return $data;
  }

  /**
   * Truncate all old posts of Timeline content type.
   * @return int
   */
  public function truncate() {
    $node_type = 'timeline';

    // Select the nodes that we want to delete.
    $result = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('type', $node_type, '=')
      ->execute();

    $deleted_count = 0;
    foreach ($result as $record) {
      node_delete($record->nid);
      $deleted_count++;
    }
    return $deleted_count;
  }

  /**
   * Get list of IDs of all events.
   */
  public function get_ids() {
    // Check if exists.
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'timeline')
      ->fieldOrderBy('field_timeline_date', 'value', 'ASC');

    $result = NULL;
    $result = $query->execute();
    return $result;
  }

  /**
   * Get all of the upcoming Timeline events from the DB.
   */
  public function get($limit = 10) {
    $type = 'timeline';
    $current_date = date("Y-m-d");

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->propertyCondition('status', 1)
      ->propertyCondition('type', array($type))
      ->fieldCondition('field_timeline_date', 'value2', $current_date, '>')
      ->fieldOrderBy('field_timeline_date', 'value', 'ASC');
    $query->range(0, $limit);
    $items = $query->execute();
    $nodes = NULL;
    if (isset($items['node'])) {
      $nodes = node_load_multiple(array_keys($items['node']));
    }
    return $nodes;
  }

  /**
   * Compare values in the DB and the Status.
   * @param $db_object
   * @param $status_object
   * @return bool
   */
  public function compare_value($db_object, $status_object, $array_string = 'value') {
    // Default: different values.
    $equal_values = 0;
    if (isset($db_object[LANGUAGE_NONE][0][$array_string]) && ($db_object[LANGUAGE_NONE][0][$array_string] == $status_object)) {
      $equal_values = 1;
    }
    if (isset($db_object) && ($db_object == $status_object)) {
      $equal_values = 1;
    }
    return $equal_values;
  }

  /**
   * Save Timeline Status.
   * @param $status
   */
  public function save($status) {
    global $user;
    
    // We only save Timeline data if it's an 'event' type. Discard everything else.
    if ($status->type !== 'event') {
      return;
    }

    // Now store into custom Node.
    $values = array(
      'type' => 'timeline',
      'uid' => $user->uid,
      'status' => 1,
      'comment' => 0,
      'promote' => 0,
    );

    // Need to clear cache or not.
    $clear_cache = 0;

    // Check if exists.
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'timeline')
      ->fieldCondition('field_timeline_uuid', 'value', $status->id)
      ->range(0, 1)
      ->addMetaData('account', user_load(1));

    $result = NULL;
    $result = $query->execute();

    $timeline_items_nids = array();
    if (isset($result['node'])) {
      $timeline_items_nids = array_keys($result['node']);
    }

    $entity = NULL;
    if (count($timeline_items_nids) == 0) {
      // Create new.
      $entity = entity_create('node', $values);
      $clear_cache = 1;
    }
    else {
      // Update existing.
      $entity = node_load($timeline_items_nids[0]);
      $clear_cache = 0;
    }

    // Create the wrapper.
    $node_wrapper = entity_metadata_wrapper('node', $entity);

    $val = (string) $status->id;
    $node_wrapper->field_timeline_uuid->set($val);

    $val = (string) $status->title;
    $node_wrapper->title->set($val);

    $val = intval($status->created / 1000);
    $node_wrapper->field_timeline_created_time->set($val);

    $val = intval($status->created / 1000);
    $node_wrapper->field_timeline_created->set($val);

    $val = intval($status->lastModified / 1000);
    $node_wrapper->field_timeline_last_modified->set($val);
    // If Last Modified date changes, clear all related cache.
    $clear_cache = ($clear_cache || !$this->compare_value($entity->field_timeline_last_modified, $val));

    $val = (string) $status->content;
    $node_wrapper->body->set(['value' => $val, 'format' => variable_get('timeline_drupal_text_format', NULL)]);
    $node_wrapper->body->summary->set(substr($val, 0, 255));

    if (isset($status->location) && !empty($status->location)) {
      $val = (string) $status->location;
      $node_wrapper->field_timeline_location->set($val);
    }

    module_load_include('inc', 'timeline_drupal');
    if (isset($status->locationId) && !empty($status->locationId)) {
      $location_tids = _timeline_drupal_add_location($status->locationId, $status->location);
      try {
        if (!empty($location_tids)) {
          $node_wrapper->field_timeline_location_ref->set($location_tids);
        }
      } catch (Exception $ex) {
        // Location reference error.
      }
    }

    // Custom Location detail text.
    if (isset($status->locationDetail) && $status->locationDetail != '') {
      $val = (string) $status->locationDetail;
      $node_wrapper->field_timeline_location_detail->set($val);
    }

    if (isset($status->urlRef) && !empty($status->urlRef)) {
      $val = (string) $status->urlRef;
      $node_wrapper->field_timeline_url->set(array('url' => (string) $status->urlRef));
    }

    // Audience field.
    if (isset($status->audience) && !empty($status->audience)) {
      $val = (string) $status->audience;
      $node_wrapper->field_timeline_audience->set(substr($val, 0, 255));
    }

    // Speaker field.
    if (isset($status->speaker) && !empty($status->speaker)) {
      $val = (string) $status->speaker;
      $node_wrapper->field_timeline_speaker->set(substr($val, 0, 255));
    }

    if (isset($status->timezone) && !empty($status->timezone)) {
      $val = $status->timezone;
      $node_wrapper->field_timeline_timezone->set($status->timezone);
      $clear_cache = ($clear_cache || !$this->compare_value($entity->field_timeline_timezone, $val));
    }
    else {
      $node_wrapper->field_timeline_timezone->set(variable_get('date_default_timezone'));
    }

    // Image field.
    if (isset($status->image) && !empty($status->image)) {
      $val = (string) $status->image;
      $node_wrapper->field_timeline_image->set(array('url' => (string) $status->image));
    }

    if (!empty($status->endTime)) {
      $date_array = array(
        'value' => _timeline_drupal_date_to_utc($status->startTime, $status->timezone),
        'value2' => _timeline_drupal_date_to_utc($status->endTime, $status->timezone),
      );
    }
    else {
      $date_array = _timeline_drupal_date_to_utc($status->startTime, $status->timezone);
    }

    // Repeated Event Date Exclusions.
    if (isset($status->exclusions) && !empty($status->exclusions)) {
      $val = serialize($status->exclusions);
      $node_wrapper->field_timeline_exclusions->set(serialize($status->exclusions));
    }

    // Date values.
    $date_values = array();

    // If repeating event.
    if (strcmp($status->frequency, 'once') !== 0) {
      // If repeat interval was not defined, use 1 (each day/week/month/...).
      if (!isset($status->interval)) {
        $status->interval = 1;
      }

      // Repeated Rule.
      if (isset($status->exclusions) && !empty($status->exclusions)) {
        $date_array['rrule'] = _timeline_drupal_create_repeat_date_string($status->frequency, $status->dayIndex, $status->untilDate, $status->interval, 'SU', $status->exclusions);
      }
      else {
        $date_array['rrule'] = _timeline_drupal_create_repeat_date_string($status->frequency, $status->dayIndex, $status->untilDate, $status->interval);
      }

      // This will return an array of dates.
      // $start_date and $end_date are in Unix timestamp format.
      $repeats = date_repeat_calc($date_array['rrule'], date('Y-m-d H:i:s', strtotime($status->startTime)), strtotime($status->untilDate), $status->exclusions, $status->timezone);
      $until_date_gmt = date('U', strtotime($status->endTime));
      $start_date_gmt = date('U', strtotime($status->startTime));
      $time_diff = $until_date_gmt - $start_date_gmt;
      foreach ($repeats as $key => $date) {
        $end_timestamp = strtotime($date) + ($time_diff);
        $end_datetime = date('Y-m-d H:i:s', $end_timestamp);
        $date_values[] = array(
          'value' => _timeline_drupal_date_to_utc($date, $status->timezone),
          'value2' => _timeline_drupal_date_to_utc($end_datetime, $status->timezone),
          'rrule' => $date_array['rrule'],
        );
      }
      $node_wrapper->field_timeline_date->set($date_values);
    }
    else {
      $date_values[] = $date_array;
      $node_wrapper->field_timeline_date->set($date_values);
    }

    // Post (scheduled) date.
    if (isset($status->postStartTime) && !empty($status->postStartTime)) {
      $date_values = array(
        'value' => _timeline_drupal_date_to_utc($status->postStartTime, $status->timezone),
      );
      if (isset($status->postEndTime) && !empty($status->postEndTime)) {
        $date_values['value2'] = _timeline_drupal_date_to_utc($status->postEndTime, $status->timezone);
      }
      $node_wrapper->field_timeline_post_date->set($date_values);
    }

    // Canceled and reason.
    if (isset($status->canceled) && ((int) $status->canceled > 0 || $status->canceled == TRUE)) {
      $val = 1;
      $node_wrapper->field_timeline_canceled->set(true);
      $val = (string) $status->canceledReason;
      $node_wrapper->field_timeline_canceled_reason->set((string) $status->canceledReason);
    }
    else {
      $val = 0;
      $node_wrapper->field_timeline_canceled->set(false);
      $node_wrapper->field_timeline_canceled_reason->set(NULL);
    }

    try {
      module_load_include('inc', 'timeline_drupal');
      $timeline_drupal_tids = _timeline_drupal_add_taxonomy($status->tagSpec, 'timeline_drupal_vocabulary');
      $node_wrapper->field_timeline_drupal_tags->set($timeline_drupal_tids);
    } catch (Exception $ex) {
      //Error while saving Taxonomies.
      throw new TimelineException($ex);
    }

    // If replying to.
    if (isset($status->replyTo) && !empty($status->replyTo)) {
      $val = (string) $status->replyTo;
      $node_wrapper->field_timeline_reply_to->set($val);
    }

    if (isset($status->entities) && !empty($status->entities)) {
      $node_wrapper->entities = serialize($status->entities);
    }

    // Save the node.
    $node_wrapper->save();

    return $clear_cache;
  }
}

/**
 * Timeline User - handles authentication, tokens, IDs.
 * Class TimelineUser
 */
class TimelineUser {

  public $id;
  public $created_time;
  public $status;

  public $client_id;
  public $client_secret;

  public $code;
  public $access_token;
  public $refresh_token;

  public $host;

  // Authenticated or not.
  public $auth;

  public function __construct($values_input = array()) {
    $values = (array) $values_input;

    // Client ID.
    if (isset($values['client_id'])) {
      $this->client_id = $values['client_id'];
    }
    // Client Secret.
    if (isset($values['client_secret'])) {
      $this->client_secret = $values['client_secret'];
    }

    // User ID.
    if (!empty($values['id'])) {
      $this->id = $values['id'];
    }

    // Host URL.
    if (isset($values['host'])) {
      $this->host = $values['host'];
    }

    // Code.
    if (isset($values['code'])) {
      $this->code = $values['code'];
    }

    // Access Token.
    if (isset($values['access_token'])) {
      $this->access_token = $values['access_token'];
    }
    // Refresh Token.
    if (isset($values['refresh_token'])) {
      $this->refresh_token = $values['refresh_token'];
    }

    // State.
    if (isset($values['state'])) {
      $this->state = $values['state'];
    }

    $this->auth = FALSE;

    if (!empty($values['uid'])) {
      $this->uid = $values['uid'];
    }
  }

  /**
   * Gets all Accounts set for the Host.
   * @param null $host
   * @return mixed
   */
  public function get_account_data($host = NULL) {
    if ($host != NULL) {
      $query = db_query('SELECT *
                       FROM {timeline_account}
                       WHERE host = :host
                       LIMIT 1',
        array(':host' => $host));
    }
    else {
      $query = db_query('SELECT *
                       FROM {timeline_account}
                       LIMIT 1');
    }
    $values = $query->fetchAssoc();
    return $values;
  }

  /**
   * Returns an array with the authentication tokens.
   *
   * @return
   *   array with the oauth token key and secret.
   */
  public function get_auth() {
    return array(
      'client_id' => $this->client_id,
      'client_secret' => $this->client_secret
    );
  }

  /**
   * Sets the authentication tokens to a user.
   *
   * @param array $values
   *   Array with 'oauth_token' and 'oauth_token_secret' keys.
   */
  public function set_auth($values) {
    $this->access_token = isset($values['access_token']) ? $values['access_token'] : NULL;
    $this->client_secret = isset($values['client_secret']) ? $values['client_secret'] : NULL;
    $this->refresh_token = isset($values['refresh_token']) ? $values['refresh_token'] : NULL;
    $this->auth = TRUE;
  }

  /**
   * Authenticates application (already set in the form).
   */
  public function store_auth() {
    $timeline = new Timeline();
    // Get Code.
    $code = $timeline->get_code_val();

    // Get State.
    $state = $timeline->get_state_val();

    // Get Access Token (if already set).
    $tokens = $timeline->get_token_val();
    $access_token = $tokens['access_token'];
    $refresh_token = $tokens['refresh_token'];

    // Import the data.
    if (isset($state) && isset($code)) {
      if ($state != NULL && $code != NULL) {

        // Store UID.
        global $user;
        $client_host = $_SERVER['SERVER_NAME'];

        // Check if app was authenticated already.
        $timelineUser = new TimelineUser();
        if (!$timelineUser->auth_exists($access_token, $refresh_token, $code)) {
          if (!empty($access_token) && !empty($code) && !empty($state)) {
            watchdog('timeline_drupal', 'App: %field authenticated.', array('%field' => $state), WATCHDOG_NOTICE);

            // Save the account
            $account = array(
              'access_token' => $access_token,
              'refresh_token' => $refresh_token,
              'state' => $state,
              'code' => $code,
              'uid' => $user->uid,
              'client_id' => variable_get('timeline_drupal_consumer_key'),
              'client_secret' => variable_get('timeline_drupal_consumer_secret'),
              'host' => $client_host
            );

            // Save the account info (if needed).
            $timelineUser = new TimelineUser();
            $timelineUser->save_auth($account);
          }
        }

        if (empty($access_token)) {
          $response = $timeline->fetch_access_token($code);

          $access_token = $timeline->get_token($response, 'access_token');
          if (!empty($access_token)) {
            variable_set('timeline_drupal_oauth_access_token_val', $access_token);
          }

          $refresh_token = $timeline->get_token($response, 'refresh_token');
          if (!empty($refresh_token)) {
            variable_set('timeline_drupal_oauth_refresh_token_val', $refresh_token);
          }

          // Store when the token expires.
          $expires_in = time() + $timeline->get_token($response, 'expires_in');
          if (!empty($expires_in)) {
            variable_set('timeline_drupal_oauth_token_expire', $expires_in);
          }

          // Save the account
          $account = array(
            'access_token' => $access_token,
            'refresh_token' => $refresh_token,
            'state' => $state,
            'code' => $code,
            'uid' => $user->uid,
            'client_id' => variable_get('timeline_drupal_consumer_key'),
            'client_secret' => variable_get('timeline_drupal_consumer_secret'),
            'host' => $client_host
          );

          watchdog('timeline_drupal', 'App: %field authenticated.', array('%field' => $state), WATCHDOG_NOTICE);

          if (!empty($access_token) && !empty($code) && !empty($state)) {
            // Save the account info (if needed).
            $timelineUser = new TimelineUser();
            $timelineUser->save_auth($account);
          }
        }
      }
    }
    $token_array = array(
      'access_token' => $access_token,
      'refresh_token' => $refresh_token,
      'code' => $code,
      'state' => $state
    );
    return $token_array;
  }

  /**
   * Checks whether the account is authenticated or not.
   *
   * @return
   *   boolean TRUE when the account is authenticated.
   */
  public function is_auth() {
    return !empty($this->access_token) && !empty($this->client_secret);
  }

  /**
   * Check if user
   * @param $access_token
   * @param $refresh_token
   * @param $code
   * @return bool
   */
  public function auth_exists($access_token, $refresh_token, $code) {
    $authenticated_apps = $this->load_accounts(array(
      ':access_token' => $access_token,
      ':refresh_token' => $refresh_token,
      ':code' => $code
    ));

    return (count($authenticated_apps) > 0) ? TRUE : FALSE;
  }

  /**
   * Store the account information (application authorization settings).
   * @param $timeline_drupal_user
   * @param bool $save_auth
   */
  public function save_auth($timeline_drupal_user, $save_auth = FALSE) {
    $values = $timeline_drupal_user;

//    if ($save_auth) {
//      $values += $timeline_drupal_user->get_auth();
//    }

    $schema = drupal_get_schema('timeline_account');
    foreach ($values as $k => $v) {
      if (!isset($schema['fields'][$k])) {
        unset($values[$k]);
      }
    }

    try {
      if (!isset($values['access_token']) && !isset($values['refresh_token'])) {
        drupal_set_message(t("App: %field cannot be authenticated! access_token or refresh_token are empty.", array('%field' => $values['state'])), 'error');
        return NULL;
      }
      // Authenticate application - delete old apps.
      db_truncate('timeline_account')
        ->execute();
      // Delete old Destination information.
      variable_del('timeline_drupal_destinations');
      // Insert new Authenticated Apps.
      db_merge('timeline_account')
        ->key(array('state' => $values['state']))
        ->fields($values)
        ->execute();
      drupal_set_message(t("App: %field authenticated.", array('%field' => $values['state'])), 'status');
      watchdog('timeline_drupal', 'App: %field authenticated.', array('%field' => $values['state']), WATCHDOG_NOTICE);

      // Delete old Timeline Posts.
      $timelineStatus = new TimelineStatus();
      $deleted = $timelineStatus->truncate();
      drupal_set_message(t('Old posts deleted (count: %count).', array('%count' => $deleted)), 'status');
      watchdog('timeline_drupal', 'Old posts deleted (count: %count).', array('%count' => $deleted), WATCHDOG_NOTICE);

    } catch (Exception $ex) {
      //Notify other modules of the timeline account save
      drupal_set_message(t('Error authenticating %field .', array('%field' => $values['state'])), 'error');
      watchdog('timeline_drupal', 'Error authenticating %field .', array('%field' => $values['state']), WATCHDOG_ERROR);
      module_invoke_all('timeline_drupal', $values);
    }
  }

  /**
   * Loads all Timeline accounts added by a Drupal user.
   *
   * This excludes Timeline accounts added automatically when e.g. pulling
   * mentions of an account from the Timeline API.
   *
   * @param array $params
   *   Additional parameters - access_token, refresh_token, host
   * @return array
   *   A list of TimelineUser objects.
   */
  public function load_accounts($params = NULL) {
    $accounts = array();

    // Additional params - search for specific entry.
    $condition = (!empty($params) && is_array($params)) ? " AND ( access_token = '" . $params[':access_token'] . "' AND refresh_token = '" . $params[':refresh_token'] . "' AND code = '" . $params[':code'] . "' ) " : '';
    if (empty($params) || count($params) == 0) {
      $params = array();
    }

    $result = db_query('SELECT *
    FROM {timeline_account}
    WHERE uid <> 0 ' . $condition . '
    ORDER BY id', $params);

    foreach ($result as $account) {
      $accounts[] = $this->load_auth($account->id);
    }
    return $accounts;
  }

  /**
   * Load specific authenticated account.
   * @param $id
   * @param bool $is_uid
   * @return null|TimelineUser
   */
  public function load_auth($id, $is_uid = TRUE) {

    // The ID is the Timeline ID.
    if ($is_uid) {
      $query = db_query('SELECT *
                       FROM {timeline_account}
                       WHERE id = :id',
        array(':id' => $id));
    }
    // Otherwise use the UserID.
    else {
      $query = db_query('SELECT *
                       FROM {timeline_account}
                       WHERE uid = :uid',
        array(':uid' => $id));
    }

    $values = $query->fetchAssoc();
    if (!empty($values)) {
      module_load_include('lib.php', 'timeline_drupal');
      $account = new TimelineUser($values);
      $account->set_auth($values);
      return $account;
    }
    return NULL;
  }

  /**
   * Deletes Authenticated Timeline Account.
   * @param $id
   */
  public function delete_auth($id) {
    $timelineUser = new TimelineUser();
    $timeline_drupal_account = $timelineUser->load_auth($id);

    if (count($timeline_drupal_account) == 1) {
      // Delete from {timeline_account}.
      $query = db_delete('timeline_account');
      $query->condition('id', $id);
      $query->execute();

      // Delete from {authmap}.
      $query = db_delete('authmap');
      $query->condition('authname', $id);
      $query->condition('module', 'timeline');
      $query->execute();

      // Delete variables (temporary ones).
      variable_del('timeline_drupal_app_name');
      variable_del('timeline_drupal_oauth_access_token_val');
      variable_del('timeline_drupal_oauth_refresh_token_val');

      drupal_set_message(t("App: %field removed.", array('%field' => $id)), 'status');
      watchdog('timeline_drupal', 'App: %field removed.', array('%field' => $id), WATCHDOG_NOTICE);
    }
  }

  /**
   * Authenticate the User.
   */
  public function authenticate() {
    if (user_access('administer timeline accounts')) {
      $client_id = variable_get('timeline_drupal_consumer_key');

      $client_url = $_SERVER['SERVER_NAME'];
      $client_state = variable_get('timeline_drupal_app_name', uniqid($client_url . '_'));

      // Define Callback URL.
      $callback_url = url('/admin/config/services/' . TIMELINE_CLIENT_CALLBACK_URL, array('absolute' => TRUE));

      $params = array(
        "client_id" => $client_id,
        "grant_type" => "client_credentials",
        "redirect_uri" => $callback_url,
        "response_type" => TIMELINE_RESPONSE_TYPE,
        "state" => $client_state
      );

      // Open the external page.
      $url = variable_get('timeline_drupal_host') . TIMELINE_AUTHORIZE_URL . '?' . _timeline_drupal_urlify($params);

      watchdog('timeline_drupal', 'Authenticating app %app.', array('%app' => $client_state), WATCHDOG_ERROR);
      drupal_goto($url);
    }
  }

  /**
   * Re-Authenticate the User.
   * @param $access_token
   * @param $refresh_token
   * @return mixed
   */
  public function reauthenticate($access_token, $refresh_token) {
    $token_params = array(
      'refresh_token' => $refresh_token,
      'client_id' => variable_get('timeline_drupal_consumer_key'),
      'client_secret' => variable_get('timeline_drupal_consumer_secret'),
      'access_token' => $access_token,
      'grant_type' => 'refresh_token',
    );

    $url = variable_get('timeline_drupal_host') . TIMELINE_TOKEN_URL;
    $timeline = new Timeline();
    $response = $timeline->curl_post($url, $token_params);

    watchdog('timeline_drupal', 'Re-authenticating app with refresh token: %val.', array('%val' => $refresh_token), WATCHDOG_NOTICE);

    // If error on refreshing token, authenticate again.
    $content = json_decode($response);

    if (isset($content->error)) {
      if ($content->error == 'invalid_client' || $content->error == 'invalid_grant') {
        watchdog('timeline_drupal', 'Failed to Re-authenticate user in order to download posts (token: %val).', array('%val' => $access_token), WATCHDOG_ERROR);
      }
    }
    return $response;
  }

  /**
   * Updates authenticated app creds.
   * @param $access_token
   * @param $refresh_token
   * @param $access_response
   * @return null
   */
  public function update_auth($access_token, $refresh_token, $access_response) {
    // Global user ID.
    global $user;

    $timeline = new Timeline();
    $access_token_new = $timeline->get_token($access_response, 'access_token');
    $refresh_token_new = $timeline->get_token($access_response, 'refresh_token');

    $update_array = array(
      'access_token' => $access_token_new,
      'refresh_token' => $refresh_token_new,
      'uid' => (int) $user->uid,
    );

    if (!empty($access_token_new) && !empty($refresh_token_new)) {

      // Update tokens.
      variable_set('timeline_drupal_oauth_access_token_val', $access_token_new);
      variable_set('timeline_drupal_oauth_refresh_token_val', $refresh_token_new);

      // Update table content.
      db_update('timeline_account')
        ->fields(
          $update_array
        )
        ->condition('access_token', $access_token, '=')
        ->execute();
    }

    watchdog('timeline_drupal', 'Updated access / refresh token in DB: %val.', array('%val' => $refresh_token), WATCHDOG_NOTICE);
    return $access_token_new;
  }

}
