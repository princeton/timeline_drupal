<?php

/**
 * @file
 * Timeline API functions
 */

/**
 * Prepare the URL from the array.
 * @param type $fields
 * @return string
 */
function _timeline_drupal_urlify($fields) {
  $fields_string = '';
  if (is_array($fields) && !empty($fields)) {
    foreach ($fields as $key => $value) {
      $fields_string .= $key . '=' . $value . '&';
    }
    rtrim($fields_string, '&');
  }
  return $fields_string;
}

/**
 * Convert Timeline DB values to Timeline API stdClass array.
 * @param $items
 * @return array
 */
function _timeline_drupal_convert_list($items) {
  $data = array();
  if (count($items) > 0 && is_array($items)) {
    foreach ($items as $item) {
      $row = new stdClass();

      // Id.
      $row->id = $item->field_timeline_uuid[LANGUAGE_NONE][0]['value'];
      $row->nid = $item->nid;

      // Body.
      $row->content = $item->body[LANGUAGE_NONE][0]['value'];
      $row->body = $row->content;
      $summary = $item->body[LANGUAGE_NONE][0]['summary'];
      $max_length = 120;
      $min_wordsafe_length = 20;
      $row->summary = truncate_utf8($summary, $max_length, TRUE, TRUE, $min_wordsafe_length);

      // Title.
      $row->title = $item->title;

      // URL on Timeline site.
      $id = $item->field_timeline_uuid[LANGUAGE_NONE][0]['value'];
      $post_link = (isset($item->field_timeline_url[LANGUAGE_NONE][0]['url'])) ? $item->field_timeline_url[LANGUAGE_NONE][0]['url'] : variable_get('timeline_drupal_host') . '/app/publish/post/' . $id;

      // URL on Drupal.
      $post_link = url(drupal_get_path_alias('node/' . $item->nid));
      if (!empty($item->field_timeline_url[LANGUAGE_NONE][0]['url'])) {
        $post_link = $item->field_timeline_url[LANGUAGE_NONE][0]['url'];
      }

      // Url (external or to Drupal node).
      $row->urlRef = $post_link;

      // Start time.
      $start_time = (isset($item->field_timeline_date[LANGUAGE_NONE][0]['value'])) ? $item->field_timeline_date[LANGUAGE_NONE][0]['value'] : NULL;
      $row->startTime = $start_time;

      // End time.
      $end_time = (isset($item->field_timeline_date[LANGUAGE_NONE][0]['value2'])) ? $item->field_timeline_date[LANGUAGE_NONE][0]['value2'] : NULL;
      $row->endTime = $end_time;

      // Location.
      $location = (isset($item->field_timeline_location[LANGUAGE_NONE][0]['value'])) ? $item->field_timeline_location[LANGUAGE_NONE][0]['value'] : NULL;
      $row->location = $location;

      // Location Detail.
      $locationDetail = (isset($item->field_timeline_location_detail[LANGUAGE_NONE][0]['value'])) ? $item->field_timeline_location_detail[LANGUAGE_NONE][0]['value'] : NULL;
      $row->locationDetail = $locationDetail;

      // Speaker
      $speaker = (isset($item->field_timeline_speaker[LANGUAGE_NONE][0]['value'])) ? $item->field_timeline_speaker[LANGUAGE_NONE][0]['value'] : NULL;
      $row->speaker = $speaker;

      // Audience
      $audience = (isset($item->field_timeline_audience[LANGUAGE_NONE][0]['value'])) ? $item->field_timeline_audience[LANGUAGE_NONE][0]['value'] : NULL;
      $row->audience = $audience;

      // Canceled and reason.
      $canceled = (isset($item->field_timeline_canceled[LANGUAGE_NONE][0]['value'])) ? $item->field_timeline_canceled[LANGUAGE_NONE][0]['value'] : 0;
      $canceled_text = ($canceled == TRUE) ? "<em>Canceled : </em>" : "";
      $row->canceled = $canceled;
      $row->canceledReason = (isset($item->field_timeline_canceled_reason[LANGUAGE_NONE][0]['value'])) ? $item->field_timeline_canceled_reason[LANGUAGE_NONE][0]['value'] : "";

      // Get Tags.
      $categories_array = (isset($item->field_timeline_drupal_tags[LANGUAGE_NONE])) ? $item->field_timeline_drupal_tags[LANGUAGE_NONE] : NULL;
      $categories = array();
      if (!empty($categories_array)) {
        foreach ($categories_array as $ca) {
          $term = taxonomy_term_load((int) $ca['tid']);
          $categories[] = $term->name;
        }
      }
      $row->tagSpec = implode(', ', $categories);

      // Get Location references.
      $locations_array = (isset($item->field_timeline_location_ref[LANGUAGE_NONE])) ? $item->field_timeline_location_ref[LANGUAGE_NONE] : NULL;
      $location_reference = array();
      if (!empty($locations_array)) {
        if (count($locations_array) > 1) {
          foreach ($locations_array as $lr) {
            $term = taxonomy_term_load((int) $lr['tid']);
            $location_reference[] = $term->name;
          }
        }
        else {
          if (count($locations_array) == 1) {
            $row->locationRef = taxonomy_term_load((int) $locations_array[0]['tid']);
          }
        }
      }
      $row->locationId = implode(', ', $location_reference);

      // Export the data.
      $data[] = $row;
    }
  }

  return $data;
}

/**
 * Returns Date in the given Timezone.
 * @param $date
 * @param $timezone
 *
 */
function _timeline_drupal_timeline_timezone_date($date, $timezone) {
  $dateFormat = 'Y-m-d H:i';

  // Timezones.
  $dataTimezone = new DateTimeZone($timezone);
  $drupalTimezone = new DateTimeZone(variable_get('date_default_timezone'));
  $gmtTimezone = new DateTimeZone('GMT');
  // DateTime.
  $myDateTime = new DateTime($date, $gmtTimezone);

  // Offsets.
  $dataOffset = $dataTimezone->getOffset($myDateTime);
  $drupalOffset = $drupalTimezone->getOffset($myDateTime);

  // Drupal timezone offset.
  $offset = $drupalOffset - $dataOffset;

  // New Date.
  $new_date = new DateTime($date, $dataTimezone);
  $new_date->setTimezone($drupalTimezone);
  $date_timezone = date($dateFormat, $new_date->format('U') + $offset);

  return $date_timezone;
}

/**
 * Return Date, from custom Timezone into UTC timezone.
 * @param $date
 * @param $timezone
 * @return bool|string
 */
function _timeline_drupal_date_to_utc($date, $timezone) {
  $dateFormat = 'Y-m-d - H:i';

  // Timezones.
  $dataTimezone = new DateTimeZone($timezone);
  $drupalTimezone = new DateTimeZone(variable_get('date_default_timezone'));
  $gmtTimezone = new DateTimeZone('GMT');

  // DateTime.
  $gmt_date = new DateTime($date, $gmtTimezone);

  // Offsets.
  $dataOffset = $dataTimezone->getOffset($gmt_date);

  $drupalOffset = $drupalTimezone->getOffset($gmt_date);
  // Drupal timezone offset.
  $offset = $drupalOffset - $dataOffset;

  // New Date.
  $date_timezone = date($dateFormat, $gmt_date->format('U') - $dataOffset - $drupalOffset);

  return $date_timezone;
}

function _timeline_drupal_date_format($date, $timezone) {
  $dateFormat = 'Y-m-d H:i:s';
  return format_date($date, 'custom', $dateFormat, $timezone);
}

/**
 * Sanitize form entry.
 * @param $data
 * @return mixed
 */
function _timeline_drupal_sanitize_edit($data, $type = '') {
  return check_plain($data);
}

/**
 * Get 'code' from URL.
 * @param type $response
 * @return type
 */
function _timeline_drupal_get_code($response) {
  if (key_exists('code', $response)) {
    return filter_xss($response['code']);
  }
  else {
    return NULL;
  }
}

/**
 * Update or Create new Taxonomy Term.
 * @param $term_data
 * @param $tid
 * @param $vid
 */
function _timeline_drupal_taxonomy_term_save($term_name, $term_id, $vid) {
  $term = new stdClass();
  $term->name = $term_name;
  $term->vid = $vid;
  $term->field_new[LANGUAGE_NONE][0]['value'] = $term_id;
  taxonomy_term_save($term);
}

/**
 * Helper returns string representation of the Day Index for repeated events.
 * @param $day_index
 * @return string
 */
function _timeline_drupal_create_repeated_date_days_string($day_index) {
  // Convert dayIndex to days.
  $day_array = array();
  foreach ($day_index as $day) {
    switch ($day) {
      case 0:
        $day_array[] = 'SU';
        break;
      case 1:
        $day_array[] = 'MO';
        break;
      case 2:
        $day_array[] = 'TU';
        break;
      case 3:
        $day_array[] = 'WE';
        break;
      case 4:
        $day_array[] = 'TH';
        break;
      case 5:
        $day_array[] = 'FR';
        break;
      case 6:
        $day_array[] = 'SA';
        break;
    }
  }
  $day_output = implode(',', $day_array);
  return $day_output;
}

/**
 * Helper returns Repeat Rule for repeated events.
 * @param $frequency
 * @param $interval
 * @param $day_index
 * @param $date_to
 * @param int $week_start
 * @param $exdate
 * @return string
 */
function _timeline_drupal_create_repeat_date_string($frequency, $day_index, $date_to, $interval = 1, $week_start = 'SU', $exdate = NULL, $rdate = NULL) {
  // If repeating event.
  $date_rrule = '';

  // Repeated events rule.
  // RRULE:FREQ=WEEKLY;INTERVAL=1;BYDAY=TU;UNTIL=20161119T045959Z;WKST=SU
  // EXDATE:20160624T040000Z,20160629T040000Z
  // RDATE:20160629T040000Z,20160629T040000Z,20160629T040000Z

  // Table: field_date_rrule.
  $date_rrule .= 'RRULE:FREQ=' . strtoupper($frequency);
  $date_rrule .= ';INTERVAL=' . $interval;
  if (isset($day_index) && !empty($day_index)) {
    $date_rrule .= ';BYDAY=' . _timeline_drupal_create_repeated_date_days_string($day_index);
  }
  if (isset($date_to) && !empty($date_to)) {
    $date_rrule .= ';UNTIL=' . date('YmdHis', strtotime($date_to)) . 'Z';
  }
  $date_rrule .= ';WKST=' . $week_start;

  // Excluding Dates.
  if (!empty($exdate)) {
    $exdate_array = array();
    foreach ($exdate as $exd) {
      $exdate_array[] = date('YmdHis', strtotime($exd));
    }
    $date_rrule .= "\nEXDATE:" . implode(',', $exdate_array);
  }

  // Recurring Dates.
  if (!empty($rdate)) {
    $rdate_array = array();
    foreach ($rdate as $rd) {
      $rdate_array[] = date('YmdHis', strtotime($rd));
    }
    $date_rrule .= "\nRDATE:" . implode(',', $rdate_array);
  }

  return $date_rrule;
}

/**
 * Add Location term into Location Taxonomy.
 * @param $location_id
 * @param $location_name
 * @param null $parent_tid
 * @return int
 */
function _timeline_drupal_add_location($location_id, $location_name, $parent_tid = NULL) {
  $tid_list = array();
  $vocabulary_machine_name = 'timeline_drupal_location';
  $vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_machine_name);
  $vocabulary_uuid_field_name = 'field_timeline_location_uuid';

  $query = new EntityFieldQuery();
  $entities = $query->entityCondition('entity_type', 'taxonomy_term')
    ->entityCondition('bundle', $vocabulary_machine_name)
    ->fieldCondition($vocabulary_uuid_field_name, 'value', $location_id, '=')
    ->execute();

  if (!empty($entities['taxonomy_term'])) {
    // Get existing terms.
    $taxonomy_terms = taxonomy_term_load_multiple(array_keys($entities['taxonomy_term']));
    $tid_list[] = intval(array_keys($taxonomy_terms)[0]);
  }
  else {
    // Add new Term.
    $new_term = new stdClass();
    $new_term->name = $location_name;
    $new_term->vid = $vocabulary->vid;

    if (!empty($parent_tid)) {
      $new_term->parent = $parent_tid;
    }

    $new_term->field_timeline_location_uuid['und']['0']['value'] = (string) $location_id;
    taxonomy_term_save((object) $new_term);
    $tid_list[] = $new_term->tid;
  }

  return $tid_list;
}

/**
 * Adds new Taxonomy Term.
 * @param $terms
 * @param $vocabulary_machine_name
 * @param $parent_id Parent Taxonomy Term
 * @return array
 */
function _timeline_drupal_add_taxonomy($terms, $vocabulary_machine_name, $parent_tid = NULL) {
  // Get Vocabulary ID.
  $vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_machine_name);
  // Generate output array of TIDs.
  $tid_list = array();
  // Vocabulary UUID field.
  $vocabulary_uuid_field_name = ($vocabulary_machine_name == 'timeline_drupal_vocabulary') ? 'field_timeline_drupal_uuid' : 'field_timeline_location_uuid';

  if (count($terms) > 0) {
    foreach ($terms as $term) {
      $field_uuid = $term->id;

      $query = new EntityFieldQuery();
      $entities = $query->entityCondition('entity_type', 'taxonomy_term')
        ->entityCondition('bundle', $vocabulary_machine_name)
        ->fieldCondition($vocabulary_uuid_field_name, 'value', $field_uuid, '=')
        ->execute();

      if (!empty($entities['taxonomy_term'])) {
        $taxonomy_terms = taxonomy_term_load_multiple(array_keys($entities['taxonomy_term']));
        $tid_list[] = intval(array_keys($taxonomy_terms)[0]);
      }
      else {
        $new_term = new stdClass();
        $new_term->name = $term->name;
        $new_term->vid = $vocabulary->vid;

        if (!empty($parent_tid)) {
          $new_term->parent = $parent_tid;
        }

        if ($vocabulary_machine_name == 'timeline_drupal_vocabulary') {
          $new_term->field_timeline_drupal_uuid['und']['0']['value'] = (string) $field_uuid;
        }
        else {
          $new_term->field_timeline_location_uuid['und']['0']['value'] = (string) $field_uuid;
        }
        taxonomy_term_save((object) $new_term);
        $tid_list[] = $new_term->tid;
      }
    }
  }

  // List of newly created Terms.
  return $tid_list;
}
