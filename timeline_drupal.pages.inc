<?php
/**
 * @file
 * Page callbacks for Timeline module.
 */

include_once('timeline_drupal.constants.inc');
include_once('timeline_drupal.inc');
include_once('timeline_drupal.lib.php');

/**
 * Timeline settings form.
 */
function timeline_drupal_admin_form($form, &$form_state) {
  // Default OAuth2 params (if set).
  global $conf;

  // Timeline external APIs settings.
  $form['timeline_drupal'] = array(
    '#type' => 'fieldset',
    '#title' => t('Timeline API Settings'),
    '#description' => t('The following settings define Timeline service API. ' .
      'Define your Timeline server and API data source.'),
  );

  // Form fields.
  $form['timeline_drupal']['timeline_drupal_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Timeline host'),
    '#description' => t('Please enter full URI of the Timeline API host (including http:// or https://)'),
    '#default_value' => variable_get('timeline_drupal_host', (isset($conf['timeline_drupal_host'])) ? $conf['timeline_drupal_host'] : ''),
  );
  // Resource location (feed).
  $form['timeline_drupal']['timeline_drupal_search'] = array(
    '#type' => 'textfield',
    '#title' => t('Timeline Data source'),
    '#description' => t('Please enter location of the data source on the Timeline API server. The data will be fetched from this location. (default: /oauth/resource ).'),
    '#default_value' => variable_get('timeline_drupal_search', TIMELINE_RESOURCE_URL),
  );
  // oAuth Settings.
  $form['oauth'] = array(
    '#type' => 'fieldset',
    '#title' => t('OAuth Settings'),
    '#description' => t('To enable OAuth based access for timeline, you must <a href="@url">register your application</a> with Timeline API and add the provided keys here.', array('@url' => variable_get('timeline_drupal_host', (isset($conf['timeline_drupal_host'])) ? $conf['timeline_drupal_host'] : ''))),
  );
  // Callback URL (on this site).
  $form['oauth']['timeline_drupal_oauth_callback_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Callback URL'),
    '#default_value' => variable_get('timeline_drupal_oauth_callback_url', TIMELINE_CLIENT_CALLBACK_URL),
    '#description' => t('There is usually no need to modify this. Defaults to "%path".', array('%path' => TIMELINE_CLIENT_CALLBACK_URL)),
  );
  // Consumer Key.
  $form['oauth']['timeline_drupal_consumer_key'] = array(
    '#type' => 'textfield',
    '#title' => t('OAuth Consumer key'),
    '#default_value' => variable_get('timeline_drupal_consumer_key', (isset($conf['timeline_drupal_consumer_key'])) ? $conf['timeline_drupal_consumer_key'] : ''),
  );
  // Consumer Secret.
  $form['oauth']['timeline_drupal_consumer_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('OAuth Consumer secret'),
    '#default_value' => variable_get('timeline_drupal_consumer_secret', (isset($conf['timeline_drupal_consumer_secret'])) ? $conf['timeline_drupal_consumer_secret'] : ''),
  );

  return system_settings_form($form);
}

/**
 * Form builder that lists Timeline accounts.
 * (/admin/config/services/timeline?code=...).
 *
 * @param object $account
 *   Optional user account.
 * @return
 *   A list of Timeline accounts and a form to add more.
 */
function timeline_drupal_user_settings($account = NULL) {
  // Authenticate accounts.

  // Authorize and save authorization info.
  $client_url = $_SERVER['SERVER_NAME'];
  $referringSite = $_SERVER['HTTP_REFERER'];
  $server_url = variable_get('timeline_drupal_host');
  if (!empty($server_url) && ($referringSite == $server_url || $referringSite == '' || strpos($referringSite, $client_url) >= 0)) {
    // Store info only if redirecting from OAuth server.
    $timelineUser = new TimelineUser();
    $params = $timelineUser->store_auth();
  }

  // Verify OAuth keys.
  if (!timeline_drupal_api_keys()) {
    $variables = array('@timeline-settings' => url('admin/config/services/timeline/settings'));
    $output = '<p>' . t('You need to authenticate at least one Timeline account in order to use the Timeline API. Please fill out the OAuth fields at <a href="@timeline-settings">Timeline Settings</a> and then return here.', $variables) . '</p>';
  }
  else {
    module_load_include('lib.php', 'timeline_drupal');

    $timelineUser = new TimelineUser();

    if (!$account) {
      $timeline_drupal_accounts = $timelineUser->load_accounts();
    }
    else {
      $timeline_drupal_accounts = timeline_drupal_timeline_drupal_accounts($account);
    }

    $output = array();
    if (count($timeline_drupal_accounts) > 0) {
      // List Timeline accounts.
      $output['header']['#markup'] = '<p>';
      if (user_access('administer timeline accounts')) {
        $variables = array('@run-cron' => url('admin/reports/status/run-cron', array('query' => array('destination' => 'admin/config/services/timeline'))));
        $output['header']['#markup'] .= t('Posts are pulled from Timeline by <a href="@run-cron">running cron</a>.', $variables) . ' ';
      }

      $output['header']['#markup'] .= '</p>';
      $output['list_form'] = drupal_get_form('timeline_drupal_account_list_form', $timeline_drupal_accounts);
    }
    else {
      // No accounts added. Inform about how to add one.
      $output['header'] = array(
        '#markup' => '<p>' . t('No Timeline accounts have been added yet. Click on the following button to add one.') . '</p>',
      );
    }

    $output['add_account'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add new Timeline account'),
      '#weight' => 5,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    if (user_access('add authenticated timeline accounts')) {
      $output['add_account']['form'] = drupal_get_form('timeline_drupal_auth_account_form');
    }
  }

  // Give a chance to other modules to alter the output.
  drupal_alter('timeline_drupal_user_settings', $output);

  return $output;
}

/**
 * Formats each Timeline account as a row within a form.
 */
function timeline_drupal_account_list_form($form, $form_state, $timeline_drupal_accounts = array()) {
  $form['#tree'] = TRUE;
  $form['accounts'] = array();

  foreach ($timeline_drupal_accounts as $timeline_drupal_account) {
    $form['accounts'][] = _timeline_drupal_account_list_row($timeline_drupal_account);
  }

  if (!empty($timeline_drupal_accounts)) {
    $form['buttons']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Remove authorization'),
    );
  }

  return $form;
}

/**
 * Themes the list of Timeline accounts.
 */
function theme_timeline_drupal_account_list_form($variables) {
  $form = $variables['form'];

  $header = array(
    t('Application'),
  );
  if (user_access('administer timeline accounts')) {
    $header[] = t('Added by');
  }

  $header = array_merge($header, array(
    t('Delete'),
  ));

  $rows = array();
  foreach (element_children($form['accounts']) as $key) {
    $element = &$form['accounts'][$key];

    $destination_names = variable_get('timeline_drupal_destinations');
    if (!empty($destination_names)) {
      $element['state']['#markup'] .= ' ( Destinations: ' . $destination_names . ' )';
    }

    $row = array(
      drupal_render($element['state']),
    );
    if (user_access('administer timeline accounts')) {
      $row[] = drupal_render($element['user']);
    }
    $row = array_merge($row, array(
      drupal_render($element['delete']),
    ));
    $rows[] = $row;
  }

  $output = theme('table', array('header' => $header, 'rows' => $rows));
  $output .= drupal_render_children($form);
  return $output;
}

/**
 * Form submit handler for altering the list of Timeline accounts.
 */
function timeline_drupal_account_list_form_submit($form, &$form_state) {
  $accounts = $form_state['values']['accounts'];
  module_load_include('lib.php', 'timeline_drupal');
  $timelineUser = new TimelineUser();

  foreach ($accounts as $account) {

    if (!empty($account['delete'])) {
      $timeline_drupal_account = $timelineUser->load_auth($account['id']);
      $timelineUser->delete_auth($account['id']);
      drupal_set_message(t('The Timeline account <em>!account</em> was deleted.',
        array('!account' => $timeline_drupal_account->uid)));
    }
  }
  drupal_set_message(t('The Timeline account settings were updated.'));
}

/**
 * Form to add an authenticated Timeline account.
 */
function timeline_drupal_auth_account_form($form, $form_state) {
  $form['timeline_drupal_app_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Application Name'),
    '#default_value' => uniqid($_SERVER['SERVER_NAME'] . '_'),
    '#description' => t('Deletes previous Application and replaces it with the new one.<br/><strong>Please do not use any spaces or special characters</strong>. For example, use your host name ("mysite" in case your site is: "mysite.com" )'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go to Timeline to add an authenticated account'),
    '#prefix' => t('Authenticated accounts can post, sign in and pull mentions. ' .
      'At least one authenticated account is needed for Timeline ' .
      'module to work.</br>'),
  );

  return $form;
}

/**
 * Form validation for adding a new Timeline account.
 */
function timeline_drupal_auth_account_form_validate($form, &$form_state) {
  $client_id = variable_get('timeline_drupal_consumer_key');
  $client_secret = variable_get('timeline_drupal_consumer_secret');

  if ($client_id == '' || $client_secret == '') {
    form_set_error('', t('Please configure your consumer key, secret key and app name at ' .
      '<a href="!url">Timeline settings</a>.', array(
      '!url' => url('admin/config/services/timeline'),
    )));
  }
}

/**
 * Form submit handler for adding a Timeline account.
 *
 * Loads Timeline account details and adds them to the user account.
 */
function timeline_drupal_auth_account_form_submit($form, &$form_state) {
  // Consumer Key.
  $client_id = variable_get('timeline_drupal_consumer_key');
  // Client URL.
  $client_url = $_SERVER['SERVER_NAME'];
  // State (Application name).
  $client_state = (isset($form_state['values']['timeline_drupal_app_name'])) ? $form_state['values']['timeline_drupal_app_name'] : variable_get('timeline_drupal_app_name', uniqid($client_url . '_'));

  // Define Callback URL.
  $callback_url = url('/admin/config/services/' . variable_get('timeline_drupal_oauth_callback_url', TIMELINE_CLIENT_CALLBACK_URL), array('absolute' => TRUE));

  $params = array(
    "client_id" => $client_id,
    "grant_type" => "client_credentials",
    "redirect_uri" => $callback_url,
    "response_type" => TIMELINE_RESPONSE_TYPE,
    "state" => $client_state
  );

  // Open the external page.
  $url = variable_get('timeline_drupal_host') . TIMELINE_AUTHORIZE_URL . '?' . _timeline_drupal_urlify($params);

  drupal_goto($url);
}

/**
 * Display Feed import results.
 * @param null $form
 * @param null $form_state
 * @return string
 */
function timeline_drupal_feed_page($form = NULL, &$form_state = NULL) {
  $timeline = new Timeline();
  // Get Code.
  $code = $timeline->get_code_val();
  // Get State.
  $state = $timeline->get_state_val();

  // Get Access Token (if already set).
  $tokens = $timeline->get_token_val();
  $access_token = $tokens['access_token'];
  $refresh_token = $tokens['refresh_token'];

  if (empty($access_token) || empty($code) || empty($state)) {
    $form['setup'] = array(
      '#type' => 'link',
      '#title' => t('Authenticate at least one app before fetching'),
      '#href' => 'admin/config/services/timeline'
    );

    return $form;
  }
  else {
    // Import the data.
    if (isset($state) && isset($code)) {
      if ($state != NULL && $code != NULL) {
        $timeline = new Timeline();
        $data = $timeline->fetch_feed($access_token, $refresh_token);

        // Display content.
        $output = _timeline_drupal_feed_table($data);
        return $output;
      }
    }
    else {
      return t('No results.');
    }
  }
}

/**
 * Helper method to draw the table with the results on the Feeds page.
 * This method simply renders the table with currently fetched items.
 * @param $data
 * @return mixed
 */
function _timeline_drupal_feed_table($data) {
  // Convert array of stdClass to simple array.
  $data = json_decode(json_encode($data), TRUE);

  $header = array();
  $rows = array();
  $table_rows = array();

  $display_array = array('id', 'title', 'startTime', 'endTime');

  if (count($data) > 0) {
    foreach ($data[0] as $key => $row) {
      if (in_array($key, $display_array)) {
        $header[] = array('data' => $key . '', 'class' => $key);
      }
    }
  }

  for ($i = 0; $i < count($data); $i++) {
    $row = (array) $data[$i];
    $row_items = array();
    foreach ($row as $key => $r) {
      if (in_array($key, $display_array)) {
        if (is_array($r)) {
          $r = json_encode($r);
        }
        $row_items[] = array('data' => $r . '', 'class' => $key);
      }
    }
    $rows[] = $row_items;
  }

  $table_rows[] = array('data' => $rows);

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**make another request.
 * Returns the form fields to manage a Timeline account.
 */
function _timeline_drupal_account_list_row($account) {
  $form['#account'] = $account;

  $form['id'] = array(
    '#type' => 'value',
    '#value' => $account->id,
  );

  $state = $account->state;
  if (!is_array($state)) {
    $state = filter_xss($state);
  }

  $form['state'] = array(
    '#markup' => $state,
  );

  if (user_access('administer timeline accounts')) {
    $user = user_load($account->uid);
    $form['user'] = array(
      '#markup' => l($user->name, 'user/' . $account->uid),
    );
  }

  $form['delete'] = array(
    '#type' => 'checkbox',
  );

  return $form;
}
